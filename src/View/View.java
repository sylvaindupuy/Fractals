package View;

import Controller.IFractals;
import Model.AFractals;
import javafx.application.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.geometry.Orientation;
import javafx.geometry.Point3D;
import javafx.scene.AmbientLight;
import javafx.scene.control.*;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import Controller.JuliaController;
import Model.MandelbrotAndJuliaModel;
import Controller.MandelbrotController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

public class View extends Application {

    // final attributes
    public final static int SCREEN_WIDTH = 1280;
    public final static int SCREEN_HEIGHT = 720;
    public final static int MENU_WIDTH = 200;
    public final static int NB_THREAD = Runtime.getRuntime().availableProcessors();

    // Controller and model
    private IFractals controller;
    private MandelbrotAndJuliaModel model;

    // Screen layout and scene
    private GridPane root;
    private Scene scene;

    // tools box items ( Menu items )
    private Spinner<Integer> iterations;
    private ComboBox<String> colors;
    private ComboBox<String> fractalName;
    private Slider r = new Slider(0, 255, 255), g = new Slider(0, 255, 255), b = new Slider(0, 255, 255);
    private Button reset = new Button("Reset");
    private Button zoomIn, zoomOut;
    private Button rotateLeft = new Button("←"), rotateRight = new Button("→");
    private Button rotateTop = new Button("↑"), rotateBottom = new Button("↓");
    private Button saveImg = new Button("Save the picture");
    private Button displayMode = new Button("Sphere");

    // Display
    private ImageView fractalImg;
    private ImageView fractalImg2;
    private ProgressIndicator progress;

    // Sphere
    private Sphere sphere;
    private PhongMaterial material;

    // Others
    private boolean inDrag = false;
    private double startPositionX, startPositionY;
    private Timer zooming;


    @Override
    public void start(Stage primaryStage) throws Exception {

        initModelController();
        initComponents(primaryStage);
        initImages();
        initSphere();
        addModelListeners();
        initRoot();
        initScene();
        addSceneEvents();

        controller.drawFractal();

        primaryStage.setTitle("Fractals - DUPUY ALLIO S3A");
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * Create a Sphere and the associated material
     */
    private void initSphere() {
        sphere = new Sphere((SCREEN_HEIGHT - 50) / 2);
        material = new PhongMaterial();
        material.setDiffuseMap(model.getWritableFractal());
        sphere.setTranslateX(((SCREEN_WIDTH - MENU_WIDTH) - sphere.getRadius() * 2) / 2);
        sphere.setMaterial(material);
        sphere.setVisible(false);
    }

    /**
     *
     */
    private void resetFractalImg() {
        controller.setWritableFractal(new WritableImage(View.SCREEN_WIDTH - MENU_WIDTH, View.SCREEN_HEIGHT));
        fractalImg.setImage(model.getWritableFractal());
        fractalImg.setVisible(true);
    }

    /**
     *
     */
    private void resetFractalImg2() {
        controller.setWritableFractal2(model.getWritableFractal());
        fractalImg2.setImage(model.getWritableFractal2());
        fractalImg2.setVisible(true);
    }

    /**
     *
     */
    private void initImages() {
        this.fractalImg = new ImageView(model.getWritableFractal());
        this.fractalImg2 = new ImageView(model.getWritableFractal2());
        this.fractalImg2.setVisible (false);
    }

    /**
     * Implements the different model listeners
     */
    private void addModelListeners() {
        model.addIterationsListener((observable, oldValue, newValue) -> iterations.getEditor().setText(Integer.toString(newValue.intValue())));
        model.addIsCalculatingOrDrawingListener(((observable, oldValue, newValue) -> {
            if (!newValue) {
                fractalImg2.setTranslateX(0);
                fractalImg2.setTranslateY(0);
                fractalImg2.setVisible(false);
            }
        }));
    }

    /**
     * Create the Model and the Controller
     */
    private void initModelController() {
        MandelbrotAndJuliaModel m = new MandelbrotAndJuliaModel();
        controller = new MandelbrotController(m);
        model = m;
    }

    /**
     * Create the scene
     */
    private void initScene() {
        scene = new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT);
        scene.setFill(Color.BLACK);
    }

    /**
     * Add reaction to the different events
     */
    private void addSceneEvents() {
        scene.setOnMousePressed(event -> {
            if (event.getClickCount() == 1 && !model.getIsCalculatingOrDrawing() && Objects.equals(displayMode.getText(), "Sphere")) {
                resetFractalImg2();
                startPositionX = event.getX() - MENU_WIDTH;
                startPositionY = event.getY();
                fractalImg.setVisible(false);
                inDrag = true;
            }
        });
        scene.setOnMouseDragged(event -> {
            if (inDrag) {
                fractalImg2.setTranslateX((event.getX() - MENU_WIDTH) - startPositionX);
                fractalImg2.setTranslateY(event.getY() - startPositionY);
            }
        });
        scene.setOnMouseReleased(event -> {
            if (inDrag) {
                resetFractalImg();
                controller.dragMove(fractalImg2.getTranslateX() / model.getPointsPerUnit(), fractalImg2.getTranslateY() / model.getPointsPerUnit());
                inDrag = false;
            }
        });

        scene.setOnScroll(event -> {

            if (!model.getIsCalculatingOrDrawing() && displayMode.getText().equals("Sphere")) {
                double deltaY = event.getDeltaY();

                ActionListener zoom = e -> {
                    controller.drawFractal();
                    zooming.stop();
                };

                if (zooming == null) {
                    zooming = new Timer(350, zoom);
                }
                if (zooming.isRunning()) {
                    zooming.restart();
                } else {
                    zooming.start();
                }

                if (deltaY < 0)
                    controller.zoomOut(event.getX() - MENU_WIDTH, event.getY());
                else
                    controller.zoomIn(event.getX() - MENU_WIDTH, event.getY());
            }
        });
    }

    /**
     * Create the menu's items and add the different associated listeners
     * @param primaryStage used for FileChooser
     */
    private void initComponents(Stage primaryStage) {
        SpinnerValueFactory.IntegerSpinnerValueFactory valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10000);
        valueFactory.setValue(100);
        iterations = new Spinner<>(valueFactory);
        iterations.setEditable(true);
        iterations.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!model.getIsCalculatingOrDrawing()) {
                controller.setIterations(newValue);
                controller.drawFractal();
            }
        });
        progress = new ProgressIndicator(0);
        progress.progressProperty().bindBidirectional(model.getProgressProperty());
        progress.setMaxWidth(25);
        progress.setTranslateX((SCREEN_WIDTH - MENU_WIDTH) / 2);

        colors = new ComboBox<>();
        ObservableList<String> items = FXCollections.observableArrayList(
                "Rainbow", "Vert", "Bleu", "Rouge", "Violet", "Perso");
        colors.setItems(items);
        colors.getSelectionModel().selectFirst();
        colors.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!model.getIsCalculatingOrDrawing()) {
                controller.setColor(newValue);
                controller.drawFractal();
            }
        });

        fractalName = new ComboBox<>();
        ObservableList<String> items2 = FXCollections.observableArrayList(
                "Mandelbrot", "Julia");
        fractalName.setItems(items2);
        fractalName.getSelectionModel().selectFirst();
        fractalName.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!model.getIsCalculatingOrDrawing()) {
                controller.reset();
                switch (newValue) {
                    case "Mandelbrot":
                        controller = new MandelbrotController(model);
                        break;
                    case "Julia":
                        controller = new JuliaController(model);
                        break;
                }
                controller.drawFractal();
            }
        });

        zoomIn = new Button("+");
        zoomOut = new Button("-");

        zoomIn.setOnAction(event -> {
            if (!model.getIsCalculatingOrDrawing()) {
                controller.zoomIn((View.SCREEN_WIDTH - View.MENU_WIDTH) / 2, View.SCREEN_HEIGHT / 2);
                controller.drawFractal();
            }
        });
        zoomOut.setOnAction(event -> {
            if (!model.getIsCalculatingOrDrawing()) {
                controller.zoomOut((View.SCREEN_WIDTH - View.MENU_WIDTH) / 2, View.SCREEN_HEIGHT / 2);
                controller.drawFractal();
            }
        });

        displayMode.setOnAction(event -> {
            if (!model.getIsCalculatingOrDrawing()) {
                if (Objects.equals(displayMode.getText(), "Sphere")) {
                    fractalImg.setVisible(false);
                    material.setDiffuseMap(model.getWritableFractal());
                    sphere.setVisible(true);
                    rotateLeft.setVisible(true);
                    rotateRight.setVisible(true);
                    rotateTop.setVisible(true);
                    rotateBottom.setVisible(true);
                    displayMode.setText("Vue en 2D");
                } else {
                    fractalImg.setVisible(true);
                    sphere.setVisible(false);
                    rotateLeft.setVisible(false);
                    rotateRight.setVisible(false);
                    rotateTop.setVisible(false);
                    rotateBottom.setVisible(false);
                    sphere.getTransforms().clear();
                    displayMode.setText("Sphere");
                }
            }
        });

        reset.setOnAction(event -> {
            if (displayMode.getText().equals("Sphere")) {
                controller.reset();
                controller.drawFractal();
            } else {
                controller.reset();
                sphere.getTransforms().clear();
                controller.drawFractal();
            }
        });

        r.valueProperty().addListener((observable, oldValue, newValue) -> {
            model.setR(newValue.intValue());
            if (!model.getIsCalculatingOrDrawing() && model.getColor().equals("Perso")) {
                controller.drawFractal();
            }
        });

        g.valueProperty().addListener((observable, oldValue, newValue) -> {
            model.setG(newValue.intValue());
            if (!model.getIsCalculatingOrDrawing() && model.getColor().equals("Perso")) {
                controller.drawFractal();
            }
        });

        b.valueProperty().addListener((observable, oldValue, newValue) -> {
            model.setB(newValue.intValue());
            if (!model.getIsCalculatingOrDrawing() && model.getColor().equals("Perso")) {
                controller.drawFractal();
            }
        });

        rotateLeft.setVisible(false);
        rotateRight.setVisible(false);
        rotateTop.setVisible(false);
        rotateBottom.setVisible(false);

        rotateLeft.setOnAction(event -> sphere.getTransforms().add(new Rotate(10, new Point3D(0, -1, 0))));

        rotateRight.setOnAction(event -> sphere.getTransforms().add(new Rotate(10, new Point3D(0, 1, 0))));

        rotateTop.setOnAction(event -> sphere.getTransforms().add(new Rotate(10, new Point3D(1, 0, 0))));

        rotateBottom.setOnAction(event -> sphere.getTransforms().add(new Rotate(10, new Point3D(-1, 0, 0))));

        saveImg.setOnAction((ActionEvent event) -> {
            if (!model.getIsCalculatingOrDrawing()) {
                FileChooser fc = new FileChooser();
                fc.setTitle("Choose a directory");
                fc.setInitialDirectory(new File(System.getProperty("user.home")));
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                        new FileChooser.ExtensionFilter("PNG", "*.png")
                );

                File outpoutFile = fc.showSaveDialog(primaryStage);

                if (outpoutFile != null) {
                    BufferedImage bImage = SwingFXUtils.fromFXImage(fractalImg.getImage(), null);
                    try {
                        ImageIO.write(bImage, "png", outpoutFile);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
    }

    /**
     * - Create the root layout and the ToolBar
     * - add the different components
     */
    private void initRoot() {
        root = new GridPane();
        root.getColumnConstraints().setAll(
                new ColumnConstraints(MENU_WIDTH, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(SCREEN_WIDTH - MENU_WIDTH, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE)
        );

        ToolBar menu = new ToolBar(
                new Separator(), new Label("Itérations :"),
                iterations,
                new Separator(), new Label("Couleur :"),
                colors,
                new Separator(), new Label("Couleur perso :"),
                new HBox(new Label("Rouge"), r), new HBox(new Label("Vert "), g), new HBox(new Label("Bleue"), b),
                new Separator(), new Label("Zoom :"),
                new HBox(zoomOut, zoomIn),
                new Separator(), new Label("Quelle fractale ?"),
                fractalName,
                new Separator(),
                reset,
                new Separator(), new Label("Enregistrer l'image :"),
                saveImg,
                new Separator(), new Label("Mode d'affichage :"),
                displayMode,
                new HBox(rotateLeft, rotateRight, rotateTop, rotateBottom)
        );

        menu.setOrientation(Orientation.VERTICAL);
        menu.setMinHeight(SCREEN_HEIGHT);
        menu.setMaxWidth(SCREEN_WIDTH);

        root.add(fractalImg, 1, 0);
        root.add(fractalImg2, 1, 0);
        root.add(sphere, 1, 0);
        root.add(progress, 1, 0);
        root.add(menu, 0, 0);
        root.add(new AmbientLight(), 1, 0);
    }

    /**
     * Launch the App
     * @param args arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
