package Model;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

public class MandelbrotAndJuliaModel extends AFractals {

    public MandelbrotAndJuliaModel() {
        super(-2.1, -1.2, 300, 70);
    }

}