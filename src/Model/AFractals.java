package Model;

import View.View;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import java.util.HashSet;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

public abstract class AFractals {

    // the different images
    private WritableImage writableFractal;
    private WritableImage writableFractal2;

    // Property
    private IntegerProperty iterations;
    private BooleanProperty isCalculatingOrDrawing;
    private LongProperty startCalculTime;
    private LongProperty calculTime;
    private DoubleProperty progress;

    // Calculation Results
    private int[][] tab;

    // Position of the fractal in the imaginary plane
    private double x1, y1;

    // use to zoom
    private double pointsPerUnit;

    // Color
    private String color;
    private int r = 255, g = 255, b = 255;

    // Calculation tools
    private HashSet<Thread> threads;
    private int nbThreadsTerminated;
    private int nbColumnsDrawn;

    AFractals(double x1, double y1, double points, int iterations) {
        this.x1 = x1;
        this.y1 = y1;
        this.color = "Rainbow";

        this.writableFractal = new WritableImage(View.SCREEN_WIDTH - View.MENU_WIDTH, View.SCREEN_HEIGHT);
        this.writableFractal2 = new WritableImage(View.SCREEN_WIDTH - View.MENU_WIDTH, View.SCREEN_HEIGHT);

        this.pointsPerUnit = points;
        this.iterations = new SimpleIntegerProperty(iterations);
        this.startCalculTime = new SimpleLongProperty();
        this.calculTime = new SimpleLongProperty();

        this.progress = new SimpleDoubleProperty();
        this.nbColumnsDrawn = 0;
        this.isCalculatingOrDrawing = new SimpleBooleanProperty(false);
        this.tab = new int[View.SCREEN_WIDTH][View.SCREEN_HEIGHT];
        this.threads = new HashSet<>();
    }

    /*************************************** SETTERS ***************************************/

    public void setTab(int x, int y, int elem) {

        try {
            tab[x][y] = elem;
        } catch (IndexOutOfBoundsException e) {
            System.out.println("size de la fenetre : " + (View.SCREEN_WIDTH - View.MENU_WIDTH) + " - " + View.SCREEN_HEIGHT);
            System.out.println(x + " * " + y + " = " + elem);
        }
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setG(int g) {
        this.g = g;
    }

    public void setR(int r) {
        this.r = r;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }

    public void setNbColumnsDrawn(int nbColumnsDrawn) {
        this.nbColumnsDrawn = nbColumnsDrawn;
    }

    public void setProgress(double progress) {
        this.progress.set(progress);
    }

    public void setIterations(int iterations) {
        this.iterations.set(iterations);
    }

    public void setStartCalculTime(long startCalculTime) {
        this.startCalculTime.set(startCalculTime);
    }

    public void setCalculTime(long calculTime) {
        this.calculTime.set(calculTime);
    }

    public void setNbThreadsTerminated(int nbThreadsTerminated) {
        this.nbThreadsTerminated = nbThreadsTerminated;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setWritableFractal(WritableImage writableFractal) {
        this.writableFractal = writableFractal;
    }

    public void setWritableFractal2(WritableImage writableFractal2) {
        this.writableFractal2 = writableFractal2;
    }

    public void setPointsPerUnit(double pointsPerUnit) {
        this.pointsPerUnit = pointsPerUnit;
    }

    public void setIsCalculatingOrDrawing(boolean b) {
        this.isCalculatingOrDrawing.set(b);
    }

    /*************************************** GETTERS ***************************************/

    public long getStartCalculTime() {
        return startCalculTime.get();
    }

    public int getNbThreadsTerminated() {
        return nbThreadsTerminated;
    }

    public String getColor() {
        return color;
    }

    public WritableImage getWritableFractal() {
        return writableFractal;
    }

    public WritableImage getWritableFractal2() {
        return writableFractal2;
    }

    public int getNbColumnsDrawn() {
        return nbColumnsDrawn;
    }

    public DoubleProperty getProgressProperty() {
        return this.progress;
    }

    public double getPointsPerUnit() {
        return pointsPerUnit;
    }

    public int getIterations() {
        return iterations.get();
    }

    public int getB() {
        return b;
    }

    public int getG() {
        return g;
    }

    public int getR() {
        return r;
    }

    public double getX1() {
        return x1;
    }

    public double getY1() {
        return y1;
    }

    public int getTab(int x, int y) {
        return tab[x][y];
    }

    public HashSet<Thread> getThreads() {
        return threads;
    }

    public double getZoomFactor() {
        return 1.1;
    }

    public PixelWriter getPixelWriter() {
        return writableFractal.getPixelWriter();
    }

    public boolean getIsCalculatingOrDrawing() {
        return isCalculatingOrDrawing.get();
    }

    /*************************************** LISTENERS ***************************************/

    public void addIterationsListener(ChangeListener<Number> cl) {
        this.iterations.addListener(cl);
    }

    /*public void addCalculTimeListener(ChangeListener<Number> cl) {
        this.calculTime.addListener(cl);
    }*/

    public void addIsCalculatingOrDrawingListener(ChangeListener<Boolean> cl) {
        this.isCalculatingOrDrawing.addListener(cl);
    }

}
