package Controller;

import javafx.scene.image.WritableImage;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

public interface IFractals {
    void drawFractal();

    void zoomIn(double mouseX, double mouseY);

    void zoomOut(double mouseX, double mouseY);

    void dragMove(double x1, double y1);

    void setIterations(int its);

    void setColor(String color);

    void reset();

    void setWritableFractal(WritableImage writableFractal);

    void setWritableFractal2(WritableImage writableFractal2);
}