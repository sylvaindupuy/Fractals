package Controller;

import Model.MandelbrotAndJuliaModel;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

public class MandelbrotController extends MandelbrotAndJuliaController {

    public MandelbrotController(MandelbrotAndJuliaModel m) {
        super(m);
    }

    @Override
    void calculation(int x, int y) {
        double z_r, z_i, c_r, c_i;

        z_r = 0;
        z_i = 0;

        c_r = (double) x / model.getPointsPerUnit() + model.getX1();
        c_i = (double) y / model.getPointsPerUnit() + model.getY1();

        int i = 0;
        do {
            double tmp = z_r;
            z_r = z_r * z_r - z_i * z_i + c_r;
            z_i = 2 * z_i * tmp + c_i;
            i = i + 1;
        } while (z_r * z_r + z_i * z_i < 4 && i < model.getIterations());

        model.setTab(x, y, i);
    }
}
