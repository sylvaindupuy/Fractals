package Controller;

import Model.MandelbrotAndJuliaModel;
import View.View;
import javafx.application.Platform;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import java.util.HashSet;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

abstract class MandelbrotAndJuliaController implements IFractals {

    // The controller have a model
    MandelbrotAndJuliaModel model;

    MandelbrotAndJuliaController(MandelbrotAndJuliaModel m) {
        this.model = m;
    }

    /**
     * Draw the column on the WritableImage
     * @param column number of the column to draw
     */
    private void drawColumn(int column) {
        for (int line = 0; line < View.SCREEN_HEIGHT; line++) {
            if (model.getTab(column, line) == model.getIterations())
                model.getPixelWriter().setColor(column, line, Color.BLACK);
            else {
                switch (model.getColor()) {
                    case "Rainbow":
                        model.getPixelWriter().setColor(column, line, Color.hsb(model.getTab(column, line) * 15, 1., 1., 1.));
                        break;
                    case "Rouge":
                        model.getPixelWriter().setColor(column, line, Color.rgb(model.getTab(column, line) * 255 / model.getIterations(), 0, 0));
                        break;
                    case "Bleu":
                        model.getPixelWriter().setColor(column, line, Color.rgb(0, 0, model.getTab(column, line) * 255 / model.getIterations()));
                        break;
                    case "Vert":
                        model.getPixelWriter().setColor(column, line, Color.rgb(0, model.getTab(column, line) * 255 / model.getIterations(), 0));
                        break;
                    case "Perso":
                        model.getPixelWriter().setColor(column, line, Color.rgb(model.getTab(column, line) * model.getR() / model.getIterations(),
                                model.getTab(column, line) * model.getG() / model.getIterations(),
                                model.getTab(column, line) * model.getB() / model.getIterations()));
                        break;
                    case "Violet":
                        model.getPixelWriter().setColor(column, line, Color.rgb(model.getTab(column, line) * 255 / model.getIterations(),
                                model.getTab(column, line) / model.getIterations(),
                                model.getTab(column, line) * 255 / model.getIterations()));
                    default:
                        break;
                }
            }
        }
        model.setNbColumnsDrawn(model.getNbColumnsDrawn() + 1);

        model.setProgress(model.getNbColumnsDrawn() / (float) (View.SCREEN_WIDTH - View.MENU_WIDTH));

        if (model.getNbColumnsDrawn() == View.SCREEN_WIDTH - View.MENU_WIDTH)
            model.setIsCalculatingOrDrawing(false);
    }

    /**
     * Launch all the calculation threads and draw the fractal
     */
    @Override
    public void drawFractal() {

        HashSet<Thread> threads = model.getThreads();

        Object[] obj = threads.toArray();
        for (Object o : obj) {
            if (o instanceof Thread) {
                ((Thread) o).interrupt();
                threads.remove(o);
            }
        }

        model.setNbColumnsDrawn(0);
        model.setIsCalculatingOrDrawing(true);
        model.setStartCalculTime(System.currentTimeMillis());
        model.setNbThreadsTerminated(0);

        for (int i = 0; i < View.NB_THREAD; i++) {
            CalculationThread t = new CalculationThread(i);
            threads.add(t);
            t.start();
        }

    }

    /**
     * Reset the position, zoom...
     */
    @Override
    public void reset() {
        model.setX1(-2.1);
        model.setY1(-1.2);
        model.setPointsPerUnit(300);
        model.setIterations(70);
    }

    /**
     * Change the attributes to zoom in (but not re-draw the fractal)
     * @param mouseX position of the mouse
     * @param mouseY position of the mouse
     */
    @Override
    public void zoomIn(double mouseX, double mouseY) {
        double X = model.getX1() + mouseX / (model.getPointsPerUnit());
        double Y = model.getY1() + mouseY / (model.getPointsPerUnit());
        double Xafter = model.getX1() + (mouseX / (model.getPointsPerUnit() * model.getZoomFactor()));
        double Yafter = model.getY1() + (mouseY / (model.getPointsPerUnit() * model.getZoomFactor()));
        model.setX1(model.getX1() + X - Xafter);
        model.setY1(model.getY1() + Y - Yafter);
        model.setIterations(model.getIterations() + 1);
        model.setPointsPerUnit(model.getPointsPerUnit() * model.getZoomFactor());
    }

    /**
     * Change the attributes to zoom out (but not re-draw the fractal)
     * @param mouseX position of the mouse
     * @param mouseY position of the mouse
     */
    @Override
    public void zoomOut(double mouseX, double mouseY) {
        if (model.getPointsPerUnit() / model.getZoomFactor() > 200) {
            double X = model.getX1() + mouseX / (model.getPointsPerUnit());
            double Y = model.getY1() + mouseY / (model.getPointsPerUnit());
            double Xafter = model.getX1() + (mouseX / (model.getPointsPerUnit() / model.getZoomFactor()));
            double Yafter = model.getY1() + (mouseY / (model.getPointsPerUnit() / model.getZoomFactor()));
            model.setX1(model.getX1() + (X - Xafter));
            model.setY1(model.getY1() + (Y - Yafter));
            model.setIterations(model.getIterations() - 1);
            model.setPointsPerUnit(model.getPointsPerUnit() / model.getZoomFactor());
        }
    }

    /**
     * Move the fractal and re-draw
     * @param x1 x offset
     * @param y1 y offset
     */
    @Override
    public void dragMove(double x1, double y1) {
        model.setX1(model.getX1() - x1);
        model.setY1(model.getY1() - y1);
        drawFractal();
    }

    /**
     * Calculate color for a given pixel
     * @param x pixel x
     * @param y pixel y
     */
    abstract void calculation(int x, int y);

    private class CalculationThread extends Thread {

        private int debut;

        CalculationThread(int debut) {
            this.debut = debut;
        }

        @Override
        public void run() {

            // Sources de l'algo de calcul : http://sdz.tdct.org/

            // fractal calculation
            for (int x = debut; x < View.SCREEN_WIDTH - View.MENU_WIDTH; x += View.NB_THREAD) {
                for (int y = 0; y < View.SCREEN_HEIGHT; y++) {
                    calculation(x, y);
                    if (Thread.currentThread().isInterrupted()) {
                        break;
                    }
                }
                int finalX = x;
                Platform.runLater(() -> drawColumn(finalX));
            }

            model.setNbThreadsTerminated(model.getNbThreadsTerminated() + 1);

            if (model.getNbThreadsTerminated() == View.NB_THREAD)
                model.setCalculTime(System.currentTimeMillis() - model.getStartCalculTime());
            // fin algo
        }
    }

    @Override
    public void setIterations(int its) {
        model.setIterations(its);
    }

    @Override
    public void setColor(String color) {
        model.setColor(color);
    }

    @Override
    public void setWritableFractal(WritableImage writableFractal) {
        model.setWritableFractal(writableFractal);
    }

    @Override
    public void setWritableFractal2(WritableImage writableFractal2) {
        model.setWritableFractal2(writableFractal2);
    }
}
