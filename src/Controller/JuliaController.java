package Controller;

import Model.MandelbrotAndJuliaModel;

/**
 * Created by Sylvain Dupuy and Vincent Allio, S3A
 */

public class JuliaController extends MandelbrotAndJuliaController {

    public JuliaController(MandelbrotAndJuliaModel m) {
        super(m);
    }

    @Override
    void calculation(int x, int y) {
        double z_r, z_i, c_r, c_i;

        c_r = 0.285;
        c_i = 0.01;

        z_r = (double) x / model.getPointsPerUnit() + model.getX1();
        z_i = (double) y / model.getPointsPerUnit() + model.getY1();

        int i = 0;
        do {
            double tmp = z_r;
            z_r = z_r * z_r - z_i * z_i + c_r;
            z_i = 2 * z_i * tmp + c_i;
            i = i + 1;
        } while (z_r * z_r + z_i * z_i < 4 && i < model.getIterations());

        model.setTab(x, y, i);
    }

}
